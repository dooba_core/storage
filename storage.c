/* Dooba SDK
 * Generic Storage Device Model
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>

// Internal Includes
#include "storage.h"

// Storage Devices
struct storage *storage_devices;

// Initialize Storage System
void storage_init()
{
	// Clear Devices
	storage_devices = 0;
}

// Initialize Storage Device
void storage_init_dev(struct storage *s, void *user, uint64_t size, storage_io_t read, storage_io_t write, storage_sync_t sync, char *name_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, name_fmt);

	// Setup Storage Device
	s->name_len = cvsnprintf(s->name, STORAGE_NAME_MAXLEN, name_fmt, ap);
	s->size = size;
	s->user = user;
	s->read = read;
	s->write = write;
	s->sync = sync;
	s->next = 0;

	// Release Args
	va_end(ap);
}

// Read Data
uint8_t storage_read(struct storage *s, uint64_t addr, void *data, uint16_t size)
{
	// Check Read
	if(s->read == 0)																	{ return 1; }

	// Read
	return s->read(s, s->user, addr, data, size);
}

// Write Data
uint8_t storage_write(struct storage *s, uint64_t addr, void *data, uint16_t size)
{
	// Check Write
	if(s->write == 0)																	{ return 1; }

	// Write
	return s->write(s, s->user, addr, data, size);
}

// Synchronize
uint8_t storage_sync(struct storage *s)
{
	// Check Sync
	if(s->sync == 0)																	{ return 1; }

	// Sync
	return s->sync(s, s->user);
}

// Register Storage Device
void storage_register(struct storage *s)
{
	// Register
	s->next = storage_devices;
	storage_devices = s;
}
