/* Dooba SDK
 * Generic Storage Device Model
 */

#ifndef	__STORAGE_H
#define	__STORAGE_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Name Max Length
#define	STORAGE_NAME_MAXLEN			16

// I/O Method Type
struct storage;
typedef	uint8_t (*storage_io_t)(struct storage *s, void *user, uint64_t addr, void *data, uint16_t size);
typedef uint8_t (*storage_sync_t)(struct storage *s, void *user);

// Generic Storage Device Structure
struct storage
{
	// Name
	char name[STORAGE_NAME_MAXLEN];
	uint8_t name_len;

	// Storage Size in Bytes
	uint64_t size;

	// User Data
	void *user;

	// Read Data
	storage_io_t read;

	// Write Data
	storage_io_t write;

	// Sync
	storage_sync_t sync;

	// Next
	struct storage *next;
};

// Storage Devices
extern struct storage *storage_devices;

// Initialize Storage System
extern void storage_init();

// Initialize Storage Device
extern void storage_init_dev(struct storage *s, void *user, uint64_t size, storage_io_t read, storage_io_t write, storage_sync_t sync, char *name_fmt, ...);

// Read Data
extern uint8_t storage_read(struct storage *s, uint64_t addr, void *data, uint16_t size);

// Write Data
extern uint8_t storage_write(struct storage *s, uint64_t addr, void *data, uint16_t size);

// Synchronize
extern uint8_t storage_sync(struct storage *s);

// Register Storage Device
extern void storage_register(struct storage *s);

#endif
